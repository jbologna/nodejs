const path = require('path');
const fs = require('fs');
const util = require('util');

/**
Looks for a file with filename using the array of directory locations supplied via paths parameter.
@param {string} filename - the filename of the file to look for
@param {array} [paths] - an array of directories to look for the file in, defaults to [process.cwd()];
@returns {string} - absolute path to filename
@throws "Cannot find" - when filename does not exist in any directory
*
*/
exports.findFileUsingPath = function(filename, paths) {
    if (!paths || (paths.constructor !== Array)) paths = [process.cwd()];
    for(var p of paths) {
        var abspath = path.resolve(p, filename);
        if (fs.existsSync(abspath)) return abspath;
    }
    throw 'Cannot find, filename: '  + filename + ' in ' + paths
}

/**
Looks for a file with filename using the array of directory locations supplied via paths parameter and returns an object parsed from the JSON file.
@param {string} filename - the filename of the file to look for
@param {array} paths - an array of directories to look for the file in
@param {object} [default_value] - a default value to return instead of throwing execeptions
@returns {object} - a JSON object
@throws "Cannot find" - when filename does not exist in any directory
@throws "Cannot read" - when the file cannot be read
@throws "Cannot parse" - when the file does not contain well formed JSON
*
*/
exports.getJSONFromFileUsingPath = function(filename, paths, default_value) {
    try {
        var abspath = exports.findFileUsingPath(filename, paths);
        try {
            var s = fs.readFileSync(filename);
            try {
                var o = JSON.parse(s);
                return o;
            } catch(e) {
                if (default_value) return default_value;
                throw 'Cannot parse, filename: ' + abspath;
            }
        } catch(e) {
            if (default_value) return default_value;
            throw e + ', Cannot read, filename: ' + abspath;
        }
    } catch(e) {
        if (default_value) return default_value;
        throw e + ', findFileUsingPath failed';
    }
}

/**
Logging modes, bit positions
*/

exports.LOG_LEVEL = {
  OFF : 0,
  WARN : 0x1,
  SESSION : 0x1 << 1,
  TRACE : 0x1 << 2
};

exports.log_level = exports.LOG_LEVEL.OFF;

exports.valid_log_level = function(level) {
  if (
    (level == exports.LOG_LEVEL.OFF) ||
    ((level & exports.LOG_LEVEL.WARN) == exports.LOG_LEVEL.WARN) ||
    ((level & exports.LOG_LEVEL.SESSION) == exports.LOG_LEVEL.SESSION) ||
    ((level & exports.LOG_LEVEL.TRACE) == exports.LOG_LEVEL.TRACE)
  ) {
      return true;
  }
  throw "level: " + level + " is not supported";
};

exports.set_log_level = function(level) {
  if (exports.valid_log_level(level)) {
    exports.log_level = level;
  }
  // never hit, throw will occur
};

exports.datefmt = function(d) {
  if (!d) d = new Date();

	return util.format("%d/%d/%d %d:%d:%d",
		d.getMonth()+1,
		d.getDay(),
		d.getFullYear(),
		d.getHours(),
		d.getSeconds(),
		d.getSeconds())
	.replace(/([ :\/])([0-9])([:\ ])/g, "$10$2$3")
	.replace(/([:\/])([0-9])([:\/])/g, "$10$2$3")
	.replace(/([ :])([0-9]$)/g, "$10$2")
	.replace(/(^[0-9])(\/)/g, "0$1$2");
}

exports.genlogmsg = function(level, msg, nodate) {
  if (exports.valid_log_level(level)) {
    if ((level & exports.log_level) != 0) {
      return (!nodate ? (exports.datefmt() + " ") : "" ) + level + ": " + msg;
    }
    return "";
  }
  // never hit, throw will occur
  return "";
};

exports.log = function(level, msg, nodate) {
  var logmsg = exports.genlogmsg(level, msg, nodate);
  if (logmsg != "") console.log(logmsg);
};
