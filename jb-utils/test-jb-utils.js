#!/usr/bin/env node
const test = require('tape');
const fs = require('fs');
const path = require('path');
const jbu = require('./index.js');

var missing_file = 'missing_file';
var test_file = 'test_file';
var testFiles = [
    test_file,
    [process.env.HOME, test_file].join(path.sep)
]

/**
Create files in all locations
@throws "Aborting..." when file in files exists to avoid removing an already existing file later in the test.
*/
function setup(files) {
    for (var file of files) {
        if (fs.existsSync(file)) {
            throw 'Aborting, file: ' + file + ' already exists. Cannot test.';
        }
        fs.closeSync(fs.openSync(file, 'w'));
    }
}

/**
Remove files in all locations, ignoring files that have already been removed.
*/
function cleanup(files) {
    for (var file of files) {
        try {
            fs.unlinkSync(file);
        } catch(e) {}
    }
}

/**
Make a non-readable file.
@param {string} file - non-readable file to create
*/
function nonReadableFile(file) {
    fs.closeSync(fs.openSync(file, 'w'));
    var wo = parseInt('0200', 8); // needed for jsdoc, sigh.
    fs.chmodSync(file, wo);
}

/**
Test jb-utils/findFileUsingPath method
@param {string} file - filename to use for testing
@param {array} paths - array of directories
@param {array} files - array of filenames to use in testing, real files are created then deleted at these paths
@example
Test 1: Find a file in $HOME, ignore file in $(pwd).
Test 2: Find a file in $(pwd) because it is missing in $HOME
Test 3: Find a file in $(pwd) with optional array of paths set to default
Test 4: findFileUsingPath() throws exception when file is missing
*/
function testFindFileUsingPath(file, paths, files) {
    test('Find files', function(t) {
        setup(testFiles);

        var usethis = jbu.findFileUsingPath(file, paths);
        // the file should be in $HOME
        t.ok(usethis == [process.env.HOME, test_file].join(path.sep));

        // remove the file in $HOME, so it will be found in $(pwd)
        fs.unlinkSync(usethis);

        var usethis = jbu.findFileUsingPath(file, paths);
        // the file should be in .
        t.ok(usethis == [process.cwd(), test_file].join(path.sep));

        var usethis = jbu.findFileUsingPath(file);
        // the file should be in .
        t.ok(usethis == [process.cwd(), test_file].join(path.sep));

        // handle missing_file with an exception
        t.throws(function() {
            var usethis = jbu.findFileUsingPath(missing_file, paths);
        }, /Cannot find/);

        cleanup(testFiles);
        t.end();
    });
}

/**
Test jb-utils/testGetJSONFromFileUsingPath method using package.json file.
@example
Test 5: File contains valid JSON
Test 6: File contains valid JSON, default paths
Test 7: File cannot be found
Test 8: File cannot be found, default returned
Test 9: File does not contain valid JSON
Test 10: File does not contain valid JSON, default returned
Test 11: File cannot be read
Test 12: File cannot be read, default returned
*/
function testGetJSONFromFileUsingPath() {
    test('Parse JSON', function(t) {
        var empty = {};
        var json = jbu.getJSONFromFileUsingPath('package.json', [process.cwd()]);
        // the file should be in $HOME
        t.ok(('name' in json) && (json.name == 'jb-utils'));

        var json = jbu.getJSONFromFileUsingPath('package.json');
        // the file should be in $HOME
        t.ok(('name' in json) && (json.name == 'jb-utils'));

        // handle file missing_file
        t.throws(function() {
            var json = jbu.getJSONFromFileUsingPath(missing_file, [process.cwd()]);
        }, /Cannot find/);

        var json = jbu.getJSONFromFileUsingPath(missing_file, [process.cwd()], empty);
        t.ok(Object.keys(json).length == Object.keys(empty).length);

        // handle parsing bad JSON with an exception
        t.throws(function() {
            var json = jbu.getJSONFromFileUsingPath('LICENSE.txt', [process.cwd()]);
        }, /Cannot parse/);

        var json = jbu.getJSONFromFileUsingPath('LICENSE.txt', [process.cwd()], empty);
        t.ok(Object.keys(json).length == Object.keys(empty).length);

        var nrf = "nonReadableFile";
        var nrfd = fs.mkdtempSync("jb-utils");
        var nrfp = [nrfd, nrf].join(path.sep);
        nonReadableFile(nrfp);

        // handle parsing bad JSON with an exception
        t.throws(function() {
            var json = jbu.getJSONFromFileUsingPath(nrf, [nrfd]);
        }, /Cannot read/);

        var json = jbu.getJSONFromFileUsingPath(nrf, [nrfd], empty);
        t.ok(Object.keys(json).length == Object.keys(empty).length);

        fs.unlinkSync(nrfp);
        fs.rmdirSync(nrfd);

        t.end();
    });
}

testFindFileUsingPath(test_file, [process.env.HOME, process.cwd()], testFiles);

testGetJSONFromFileUsingPath();

function testDateFmt() {
  test('Test datefmt', function(t) {
    t.ok(jbu.datefmt() != "");
    t.ok(jbu.datefmt(new Date()) != "");
    t.end();
  });
}

testDateFmt();

function testLogging() {
  test('Test Logging', function(t) {
    t.throws(function() {
      jbu.set_log_level(0x1 << 3);
    }, /level: /);

    jbu.set_log_level(jbu.LOG_LEVEL.WARN);
    var msg = "warning";
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.WARN, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.WARN + ": " + msg));

    jbu.set_log_level(jbu.LOG_LEVEL.SESSION);
    var msg = "session";
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.SESSION, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.SESSION + ": " + msg));

    jbu.set_log_level(jbu.LOG_LEVEL.TRACE);
    var msg = "trace";
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.TRACE, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.TRACE + ": " + msg));

    jbu.set_log_level(jbu.LOG_LEVEL.WARN|jbu.LOG_LEVEL.TRACE);
    var msg = "warning|trace";
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.OFF, msg) == "");
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.WARN, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.WARN + ": " + msg));
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.TRACE, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.TRACE + ": " + msg));

    jbu.set_log_level(jbu.LOG_LEVEL.WARN|jbu.LOG_LEVEL.SESSION|jbu.LOG_LEVEL.TRACE);
    var msg = "warning|session|trace";
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.OFF, msg) == "");
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.WARN, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.WARN + ": " + msg));
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.SESSION, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.SESSION + ": " + msg));
    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.TRACE, msg) == (jbu.datefmt() + " " + jbu.LOG_LEVEL.TRACE + ": " + msg));

    t.ok(jbu.genlogmsg(jbu.LOG_LEVEL.TRACE, msg, true) == (jbu.LOG_LEVEL.TRACE + ": " + msg));

    t.end();
  });
}

testLogging();
